function isPrimeNumber(x) {
  var isPrime = true;
  if (x < 2) return false;
  else if (x > 2)
    for (var i = 2; i <= Math.floor(x / 2); i++) {
      if (x % i == 0) {
        return false;
      }
    }
  return true;
}
function inSo() {
  var n = document.getElementById("txt_so").value * 1;
  var s = "";
  for (var i = 1; i <= n; i++) {
    if (isPrimeNumber(i) == true) s += `${i} `;
  }
  document.getElementById("result").innerText = s;
}
